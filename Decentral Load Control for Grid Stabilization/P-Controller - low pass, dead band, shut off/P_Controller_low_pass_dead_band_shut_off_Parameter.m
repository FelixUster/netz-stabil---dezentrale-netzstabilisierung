%%Power Grid Simulation parameters
f_0 = 50; % in Hz nominal frequency
S_B = 232*10^9; %in W; % http://appsso.eurostat.ec.europa.eu/nui/show.do?dataset=nrg_105m&lang=en -> 2018M08 -> Europe 19 -> Wert durch (31 * 24) teilen um auf einen durchschnittlichen Mittelwert zu kommen
H = 6; % in s rotational inertia % Ist der derzeitige, offizielle Wert aus den ENTSO-E Dokumenten, in der Realit�t weniger, da mehr auf erneuerbare Energie gesetzt wird. Realistisch sind wohl derzeit was bei 4-5s

T_td = 5; %s ( Delay of the turbines (of primary and secondary control))


% Incident
% https://www.entsoe.eu/fileadmin/user_upload/_library/publications/entsoe/Operation_Handbook/Policy_1_final.pdf
% A-D3.2.
P_diff = 3000e6; %W ENTSOE: reference incident
%P_diff = 1000e6; %W ENTSOE: observation incident
%P_diff = 600e6; %W ENTSOE: weakest marked incident

T_step = 50 ; % Time when the incident happends

P_noise = 15e8;  % Load Power noise
T_noise = 0.02; % Noise Sample Time


%% Grid Self Regulating Effect
SRE = 0.01; %in %/Hz

%% Primary Control
NPFC = 19500*10^6; %Network Power Frequency Characteristics in W/Hz
f_prim_deadband = 0.02; % [HZ]
f_prim_max = 0.2; % [HZ]
P_prim = 3000*10^6; %W
T_prim = 30; %s

%% Secondary Control 
% https://www.entsoe.eu/fileadmin/user_upload/_library/publications/ce/oh/Policy1_final.pdf
% B-S3.2. Controller  Cycle  Time
T_cd = mean([2 5]); %in s ENTSOE: max. 5 s

% https://www.entsoe.eu/fileadmin/user_upload/_library/publications/entsoe/Operation_Handbook/Policy_1_final.pdf
% B-D5.1.

P_sec = sqrt(10*10^6 * ( 1.5 * S_B ) - (150*10^6)^2) - 150*10^6; %Faktor 1.5, annahme, weil eigentlich die maximal zu erwartende Last hier eingetragen werden soll, und S_B die "reale" Last wiedergibt
P_sec = P_sec * 2;  % Vorherige Zeile ist zur Bestimmung der minimalen Sekund�r-regelungsreserve gedacht. Da keine genaueren Angaben gefunden wurde: Faktor 2

% https://www.entsoe.eu/fileadmin/user_upload/_library/publications/ce/oh/Policy1_final.pdf
% B-G1.1. Controller Type and Characteristic
T_sec = mean([50 200]);   % Bestimmung dieses Parameters ist tats�chlich etwas komplexer, da dieser Wert iA zwischen 50s und 200s liegt, aber automatisch angepasst werden kann, beim auftreten von Vorf�llen

% https://www.entsoe.eu/fileadmin/user_upload/_library/publications/ce/oh/Policy1_final.pdf
% B-G1.1. Controller Type and Characteristic
C_p = -mean([0 0.5]); % ENTSOE: 0 - 0.5
C_i = -1/T_sec;

%% General Frequency Based Consumer Consumer Control
ASRE = 0.001;        % artificial self regulating effect

PowerFactor_upper = 0.1;
PowerFactor_lower = -0.1;

%% Specific Frequency Based Consumer Consumer Control
LowPassEdgeFrequency = 2*pi*0.12500;

bound_a = 0.150;
bound_b = 0.05;
bound_c = PowerFactor_upper;

shutoff_f = -0.25;
shuton_f = -0.1;