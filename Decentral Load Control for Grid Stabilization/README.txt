Die in dieser Stromnetzsimulation verwendeten Netzmodelle sind eine Reimplementierung der Arbeit von [1].  
Die Parametrisierung erfolgte im Allgemeinen anhand der offiziellen Angaben der European Network of Transmission System Operators for Electricity (ENTSO-E) ([2][3]) und der Eurostat ([4]).  
Das Simulationsmodell "Single P-Controller" beinhaltet eine Verbraucher Last Regelung nach der Arbeit von [5]

[1] A. Ulbig, T. S. Borsche, and G. Andersson, “Impact of low rotational inertia on power system stability and operation,” 2014, submitted on 22 December 2013, Last revised 26 October 2014.  
[2] ENTSO-E, “P1 - Policy 1: Load-Frequency Control and Performance,” European Network of Transmission System Operators for Electricity, Tech. Rep., 2009.  
[3] ENTSO-E, “Appendix 1 - Load-Frequency Control and Performance,” European Network of Transmission System Operators for Electricity, Tech. Rep., 2004.  
[4] Eurostat,  “Supply  of  electricity  -  monthly  data,”  European  Statistical Office, Tech. Rep., 2018, europe 19 (UCTE), 08 September 2018.  
[5] Trudnowski, Donnelly, and Lightner, “Power-system frequency and stability control using decentralized intelligent loads,” in 2005/2006 IEEE/PES Transmission and Distribution Conference and Exhibition, May 2006, pp. 1453–1459.  
