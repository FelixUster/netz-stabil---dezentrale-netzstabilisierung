#!/bin/bash

while getopts "di" args; do
#	echo "Args: [$args]"
	case $args in
		d)
			SELECT=".de"
		;;
		i)
#			echo 12
			SELECT=".info"
		;;
	esac
done

#echo $SELECT

case $SELECT in
	".de")
		curlOutput=$(curl "https://netzfrequenzmessung.de:9080/frequenz01.xml?${RANDOM}" -c cookiejar.de -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0' -H 'Accept: */*' -H 'Accept-Language: de,en-US;q=0.7,en;q=0.3' --compressed -H 'Content-Type: text/plain' -H 'Origin: https://www.netzfrequenzmessung.de' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Referer: https://www.netzfrequenzmessung.de/' 2>&1)

#		DATE=$(echo $curlOutput | grep -Eo '<z>.*</z>' | sed -e 's/<z>//' -e 's/<\/z>//' -e 's/^ *//')
#		TIME=$(echo $DATE | cut -d" " -f2 | awk -F ":" '{print $1+1":"$2":"$3}')
#		DATE=$(echo $DATE | cut -d" " -f1 | awk -F "." '{print $3"-"$2"-"$1}')

		FREQUENCY=$(echo $curlOutput | grep -o '<f>.*</f>' | sed -e 's/<f>//' -e 's/<\/f>//' -e 's/^ *//')
		FREQUENCY=${FREQUENCY:=50.0}
		if [[ "$FREQUENCY" == "0.000" ]]; then
			FREQUENCY="50.0"
		fi
		FREQUENCY=$( echo "(($FREQUENCY -50 ) * 1000)" | bc | sed -e 's/-\.0*/0.0/' -e 's/\.0*/.0/' -e 's/^0$/0.0/')
	;;
	".info")
#		curlOutput=$(curl 'https://www.netzfrequenz.info/json/act.json' -c cookiejar.info -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x8rv:70.0) Gecko/20100101 Firefox/70.0' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Accept-Language: de,en-US;q=0.7,en;q=0.3' --compressed -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' -H 'Referer: https://www.netzfrequenz.info/charts/sidebar.htm' -H 'Cookie: _pk_id.1.1e2f=6246f38829f22328.1574934767.1.1574934769.1574934767.; _pk_ref.1.1e2f=%5B%22%22%2C%22%22%2C1574934767%2C%22https%3A%2F%2Fwww.google.com%2F%22%5D; _pk_ses.1.1e2f=*; _ga=GA1.2.1348747805.1574934767; _gid=GA1.2.1143267958.1574934767; _gat=1; __utma=33223713.1348747805.1574934767.1574934767.1574934767.1; __utmb=33223713.2.9.1574934769320; __utmc=33223713; __utmz=33223713.1574934767.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt=1' -H 'If-Modified-Since: Thu, 28 Nov 2019 09:53:10 GMT' -H 'If-None-Match: W/"5-5986514c17d5d"' 2>/dev/null)
		curlOutput=$(curl 'https://www.netzfrequenz.info/json/act.json' -c cookiejar.info 2>/dev/null)
	#	echo "curl: $curlOutput"
		FREQUENCY=$(echo "(($curlOutput -50) *1000 )" | bc | sed -e 's/-\.0*/0.0/' -e 's/\.0*/.0/' -e 's/^0$/0.0/')
esac

FREQUENCY=${FREQUENCY:=0.0}

echo "$FREQUENCY"
