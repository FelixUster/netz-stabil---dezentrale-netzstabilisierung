dlc - Python Tool, that uses a grid frequency measurement to upper cpu P-state boundary
default.conf - configuration file to adapt the operation of the dlc tool. change the frequency detection device, etc.

dlc.service - systemD file to start and manage dlc
getCurrentOnlineGridFrequency.sh - mostly for debug purposes, it utilizes one of two online services to compare the measured grid frequency to another measurement
20_DecentralLoadControlFrequencyDetection.rules - udev rules to rename a frequency measurent devices for simpler usage

This service was initially designed to be used with an Intel Core Processor.
For our tests we used an Intek GPM-8213 Power meter to measure the power consumption and the power grid frequency of our test device with an Intel Core i7 9700.