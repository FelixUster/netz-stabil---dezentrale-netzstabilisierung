This Projects bases on the data center described in Data center power model based on: Rahmani, Rasoul & Moser, I. & Seyedmahmoudian, Mehdi. (2018). A Complete Model for Modular Simulation of Data Centre Power Load. arXiv:1804.00703 

We extended this model to support the power grid stabilization process by manipulating the power consumption, based on the grid frequency to adapt the maximum Server's processor power consumption.


We published the resutls in:
Felix Uster, Franz Plocksties, Dirk Timmermann, "Decentral load control for data centers", GreenCom 2020

https://www.imd.uni-rostock.de/team/mitarbeitende/persoenliche-seiten/felix-uster/
